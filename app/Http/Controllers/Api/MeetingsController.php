<?php
# app/Http/Controllers/Api/OrdersController.php

namespace App\Http\Controllers\Api;

use App\Model\Database\Meeting;
use DB;
use NilPortugues\Laravel5\JsonApi\Controller\JsonApiController;

class MeetingsController extends JsonApiController
{

    /**
     * Return the Eloquent model that will be used
     * to model the JSON API resources.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getDataModel()
    {
        return new Meeting();
    }

    // Meetings - return a list of all meetings that are available for the current day
    public function getMeetingsByDate($race_type = 'all', $date = '')
    {
        // Set today's date as default date
        if ($date == '') {
            $date = date("Y-m-d");
        }

        $meetingsQuery = Meeting::where('date', $date);

        // Accept optional race_id parameter and add it to where clause
        if ($race_type != 'all') {
            $meetingsQuery->where('meetings.meeting_type', '=', $race_type);
        }
        $rsMeetingsCollection = $meetingsQuery->orderBy('track_country')->get();


        return $rsMeetingsCollection->map(function ($rsMeeting) use ($date) {
            // Format date parameter
            $date_format = explode('-', $date);
            $date_year = isset($date_format[0]) ? $date_format[0] : date("Y");
            $date_month = isset($date_format[1]) ? $date_format[1] : date("m");
            $date_date = isset($date_format[2]) ? $date_format[2] : date("d");
            $selected_date = $date_date . '/' . $date_month . '/' . $date_year;

            // Get races for the selected date
            $races = $rsMeeting->races()->get();

            if ($date != '') {
                $races = $races->filter(function ($item) use ($selected_date) {
                    if (preg_match('~' . $selected_date . '~', $item->start_time)) {
                        return true;
                    }
                });
            }

            // Push races to meetings array
            $meetings['meeting_date'] = $rsMeeting->date;
            $meetings['meeting_name'] = $rsMeeting->track_name;
            $meetings['meeting_id'] = $rsMeeting->meeting_id;
            $meetings['meeting_country'] = $rsMeeting->track_country;
            $meetings['race_type'] = $rsMeeting->meeting_type;

            // Format the final array with race details
            $meetings['races'] = $races->map(function ($race) use ($rsMeeting) {
                $race_no = $race['race_number'];
                $races[$race_no]['race_number'] = $race_no;
                $races[$race_no]['race_type'] = $rsMeeting->meeting_type;
                $races[$race_no]['race_name'] = $race['race_name'];
                $races[$race_no]['race_starttime'] = $race['race_starttime'];
                $races[$race_no]['race_id'] = $race['race_id'];
                $races[$race_no]['race_status'] = $race['race_status'];

                return $races;
            });

            return $meetings;

        })->toArray();
    }

}
