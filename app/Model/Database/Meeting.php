<?php namespace App\Model\Database;

use Illuminate\Database\Eloquent\Model;


class Meeting extends Model
{
    public $timestamps = true;
    protected $table = 'meetings';
    protected $primaryKey = 'meeting_id';


    public function races()
    {
        return $this->hasMany(Races::class);
    }

    public function tracks()
    {
        return $this->belongsTo(Track::class, 'track_id', 'id');
    }
}
