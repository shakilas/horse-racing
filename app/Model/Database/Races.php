<?php namespace App\Model\Database;

use Illuminate\Database\Eloquent\Model;


class Races extends Model
{
    public $timestamps = true;
    protected $table = 'races';
    protected $primaryKey = 'race_no';


    /**
     * Get the meeting that owns the race
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function meetings()
    {
        return $this->belongsTo(Meeting::class, 'meeting_id');
    }
}
