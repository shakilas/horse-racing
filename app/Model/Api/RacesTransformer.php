<?php namespace App\Model\Api;

use App\Model\Database\Races;
use NilPortugues\Api\Mappings\JsonApiMapping;

class RacesTransformer extends BaseTransformer
{
    /**
     * {@inheritDoc}
     */
    public function getClass()
    {
        return Races::class;
    }

    /**
     * {@inheritDoc}
     */
    public function getAlias()
    {
        return 'race';
    }

    /**
     * {@inheritDoc}
     */
    public function getAliasedProperties()
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    public function getHideProperties()
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    public function getIdProperties()
    {
        return ['id'];
    }

    /**
     * {@inheritDoc}
     */
    public function getUrls()
    {

        $prefix = $this->getRouteNamePrefix();

        return [
            'self' => ['name' => $prefix . 'races.show', 'as_id' => 'id'],
            'races' => ['name' => $prefix . 'meetings.show', 'as_id' => 'meeting_id'],

        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getRelationships()
    {
        return [];
    }

    /**
     * List the fields that are mandatory in a persitence action (POST/PUT).
     * If empty array is returned, all fields are mandatory.
     */
    public function getRequiredProperties()
    {
        return [];
    }
} 
