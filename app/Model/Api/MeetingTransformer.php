<?php namespace App\Model\Api;

use App\Model\Database\Meeting;
use NilPortugues\Api\Mappings\JsonApiMapping;

class MeetingTransformer extends BaseTransformer
{
    /**
     * {@inheritDoc}
     */
    public function getClass()
    {
        return Meeting::class;
    }

    /**
     * {@inheritDoc}
     */
    public function getAlias()
    {
        return 'meeting';
    }

    /**
     * {@inheritDoc}
     */
    public function getAliasedProperties()
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    public function getHideProperties()
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    public function getIdProperties()
    {
        return ['meeting_id'];
    }

    /**
     * {@inheritDoc}
     */
    public function getUrls()
    {


        $prefix = $this->getRouteNamePrefix();

        return [
            'self' => ['name' => $prefix . 'meetings.show', 'as_id' => 'meeting_id'],
            //'races' => ['name' => 'api.v1.races.show', 'as_id' => 'meeting_id'],

        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getRelationships()
    {
        return [];
    }

    /**
     * List the fields that are mandatory in a persitence action (POST/PUT).
     * If empty array is returned, all fields are mandatory.
     */
    public function getRequiredProperties()
    {
        return [];
    }
} 
