<?php
namespace App\Jobs;

use App\Model\Database\Price;
use Illuminate\Contracts\Queue\Job as LaravelJob;


class ProcessPriceMessages extends Job
{

    protected $data;

    /**
     * @param LaravelJob $job
     * @param array $data
     */
    public function handle(LaravelJob $job, array $data)
    {

        // Read the XML
        $xmlString = (gzdecode(base64_decode($data['Message'])));
        $xml = simplexml_load_string(preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $xmlString));

        try {

            // -------------------------------------------------------------------
            // # # # # # # Process basic data # # # # # #
            // -------------------------------------------------------------------
            $meeting_id = isset($xml->MeetingID) ? $xml->MeetingID : '';
            $race_no = isset($xml->RaceNo) ? $xml->RaceNo : '';
            $price_source = isset($xml->Source) ? $xml->Source : '';
            $price_type = isset($xml->PriceType) ? $xml->PriceType : '';
            $pool_size = isset($xml->PoolSize) ? $xml->PoolSize : '';

            // -------------------------------------------------------------------
            // # # # # # # Process runners data # # # # # #
            // -------------------------------------------------------------------

            foreach ($xml->Runners->children() as $runner_xml) {
                $runner_tab_no = isset($runner_xml['TabNo']) ? $runner_xml['TabNo'] : '';
                $runner_name = isset($runner_xml['Name']) ? $runner_xml['Name'] : '';
                $runner_price = isset($runner_xml['Price']) ? $runner_xml['Price'] : '';

                // Check the database for the price record
                $rsPrice = Price::where('meeting_id', $meeting_id)
                    ->where('race_no', $race_no)
                    ->where('source', $price_source)
                    ->where('tab_no', $runner_tab_no)
                    ->first();

                if(count($rsPrice) == 0){

                    // New price. Insert the record.
                    $price = new Price;
                    $price->meeting_id = $meeting_id;
                    $price->race_no = $race_no;
                    $price->source = $price_source;
                    $price->price_type = $price_type;
                    $price->pool_size = $pool_size;
                    $price->tab_no = $runner_tab_no;
                    $price->name = $runner_name;
                    $price->price = $runner_price;
                    $price->status = 1;
                    $price->created_at = date("Y-m-d H:i:s");
                    $price->save();

                }else{

                    // Update existing price records
                    Price::where('meeting_id', $meeting_id)
                        ->where('race_no', $race_no)
                        ->where('source', $price_source)
                        ->where('tab_no', $runner_tab_no)
                        ->update([
                            'price_type' => $price_type,
                            'pool_size' => $pool_size,
                            'name' => $runner_name,
                            'price' => $runner_price,
                            'updated_at' => date("Y-m-d H:i:s")
                        ]);

                }

            }

        }catch (\Exception $e){
            echo 'xxxxxxxxx Unexpected error occured xxxxxxxxx';
        }
    }
}
