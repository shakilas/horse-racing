<?php

/**
 * List of queues and their corresponding handling classes
 */
return [
    'handlers' => [
        'acceptance' => App\Jobs\ProcessAcceptanceMessages::class,
        'price' => App\Jobs\ProcessPriceMessages::class,
        'open-status' => App\Jobs\ProcessOpenStatusMessages::class,
        'scratchings' => App\Jobs\ProcessResultMessages::class,
    ],

    'default-handler' => App\Jobs\ProcessAcceptanceMessages::class
];
